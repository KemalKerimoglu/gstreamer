#if 0
#include <gst/gst.h>
#include <QDebug>
#define INPUT_FILE  "/home/kemal/Downloads/video7.264"

static gboolean
bus_call (GstBus *bus,GstMessage *msg,gpointer data)
{
	GMainLoop *loop = (GMainLoop*)data;

	switch (GST_MESSAGE_TYPE (msg)) {
	case GST_MESSAGE_EOS:
		g_print ("End-of-stream\n");
		g_main_loop_quit (loop);
		break;
	case GST_MESSAGE_ERROR: {
		gchar *debug = NULL;
		GError *err = NULL;

		gst_message_parse_error (msg, &err, &debug);

		g_print ("Error: %s\n", err->message);
		g_error_free (err);

		if (debug) {
			g_print ("Debug details: %s\n", debug);
			g_free (debug);
		}

		g_main_loop_quit (loop);
		break;
	}
	default:
		break;
	}

	return TRUE;
}



#include "iostream"
gint
main (gint   argc,
	  gchar *argv[])
{
	GstStateChangeReturn ret;
	GstElement *pipeline, *filesrc,*parser,*filter,*decoder, *sink;
	GMainLoop *loop;
	GstCaps *caps1;
	GstBus *bus;
	guint watch_id;
	const gchar *input_file = INPUT_FILE;


	// initialization
	gst_init (&argc, &argv);
	loop = g_main_loop_new (NULL, FALSE);

	// create elements.
	pipeline = gst_pipeline_new ("my_pipeline");

	bus = gst_pipeline_get_bus (GST_PIPELINE (pipeline));
	watch_id = gst_bus_add_watch (bus, bus_call, loop);
	gst_object_unref (bus);

	filesrc  = gst_element_factory_make ("filesrc", "my_filesource");
	parser = gst_element_factory_make ("h264parse", "h264-parser");
	decoder  = gst_element_factory_make ("avdec_h264", "my_decoder");
	filter = gst_element_factory_make("capsfilter", "filter");
	sink     = gst_element_factory_make ("xvimagesink", "filesink");


	if (!sink ||!parser ||!decoder|| !filter|| !filesrc) {
		g_print ("Decoder or output could not be found - check your install\n");
		return -1;

	}

	caps1 = gst_caps_new_simple("video/x-raw",

								"format", G_TYPE_STRING, "I420",

								"width", G_TYPE_INT, 1920,

								"height", G_TYPE_INT, 1080,

								"framerate", GST_TYPE_FRACTION, 0, 1,

								NULL);


	g_object_set(G_OBJECT(filter), "caps", caps1, NULL);

	gst_object_unref(caps1);


	g_object_set (G_OBJECT (filesrc), "location", input_file, NULL);

	gst_bin_add_many (GST_BIN (pipeline),filesrc,parser,decoder,filter,  sink, NULL);

	// link everything together

	if (gst_element_link_many (filesrc,parser, decoder,filter,sink ,NULL) != TRUE) {
		g_print ("Failed to link one or more elements!\n");
		return -1;
	}


	// run
	ret = gst_element_set_state (pipeline, GST_STATE_PLAYING);
	if (ret == GST_STATE_CHANGE_FAILURE) {
		GstMessage *msg;

		g_print ("Failed to start up pipeline!\n");

		// check if there is an error message with details on the bus
		msg = gst_bus_poll (bus, GST_MESSAGE_ERROR, 0);
		if (msg) {
			GError *err = NULL;

			gst_message_parse_error (msg, &err, NULL);
			g_print ("ERROR: %s\n", err->message);
			g_error_free (err);
			gst_message_unref (msg);
		}
		return -1;
	}

	g_main_loop_run (loop);

	// clean up
	gst_element_set_state (pipeline, GST_STATE_NULL);
	gst_object_unref (pipeline);
	g_source_remove (watch_id);
	g_main_loop_unref (loop);

	return 0;
}
#else
#include <gst/gst.h>
#include <QDebug>
#define INPUT_DEVICE  "/dev/video0"

static gboolean
bus_call (GstBus *bus,GstMessage *msg,gpointer data)
{
	GMainLoop *loop = (GMainLoop*)data;

	switch (GST_MESSAGE_TYPE (msg)) {
	case GST_MESSAGE_EOS:
		g_print ("End-of-stream\n");
		g_main_loop_quit (loop);
		break;
	case GST_MESSAGE_ERROR: {
		gchar *debug = NULL;
		GError *err = NULL;

		gst_message_parse_error (msg, &err, &debug);

		g_print ("Error: %s\n", err->message);
		g_error_free (err);

		if (debug) {
			g_print ("Debug details: %s\n", debug);
			g_free (debug);
		}

		g_main_loop_quit (loop);
		break;
	}
	default:
		break;
	}

	return TRUE;
}


#include "iostream"
gint
main (gint   argc,
	  gchar *argv[])
{
	GstStateChangeReturn ret;
	GstElement *pipeline, *source,*filter, *sink;
	GMainLoop *loop;
	GstCaps *caps1;
	GstBus *bus;
	guint watch_id;
	const gchar *input_device = INPUT_DEVICE;


	// initialization
	gst_init (&argc, &argv);
	loop = g_main_loop_new (NULL, FALSE);

	// create elements.
	pipeline = gst_pipeline_new ("my_pipeline");

	bus = gst_pipeline_get_bus (GST_PIPELINE (pipeline));
	watch_id = gst_bus_add_watch (bus, bus_call, loop);
	gst_object_unref (bus);

	source  = gst_element_factory_make ("v4l2src", "my_filesource");
	filter = gst_element_factory_make("capsfilter", "filter");
	sink     = gst_element_factory_make ("xvimagesink", "filesink");


	if (!sink || !filter|| !source) {
		g_print ("Decoder or output could not be found - check your install\n");
		return -1;

	}

	caps1 = gst_caps_new_simple("video/x-raw",

								"format", G_TYPE_STRING, "YUY2",

								"width", G_TYPE_INT, 640,

								"height", G_TYPE_INT, 480,

								"framerate", GST_TYPE_FRACTION, 30, 1,

								NULL);


	g_object_set(G_OBJECT(filter), "caps", caps1, NULL);

	gst_object_unref(caps1);


	g_object_set (G_OBJECT (source), "device", input_device, NULL);

	gst_bin_add_many (GST_BIN (pipeline),source,filter,  sink, NULL);

	// link everything together

	if (gst_element_link_many (source,filter,sink ,NULL) != TRUE) {
		g_print ("Failed to link one or more elements!\n");
		return -1;
	}


	// run
	ret = gst_element_set_state (pipeline, GST_STATE_PLAYING);
	if (ret == GST_STATE_CHANGE_FAILURE) {
		GstMessage *msg;

		g_print ("Failed to start up pipeline!\n");

		// check if there is an error message with details on the bus
		msg = gst_bus_poll (bus, GST_MESSAGE_ERROR, 0);
		if (msg) {
			GError *err = NULL;

			gst_message_parse_error (msg, &err, NULL);
			g_print ("ERROR: %s\n", err->message);
			g_error_free (err);
			gst_message_unref (msg);
		}
		return -1;
	}

	g_main_loop_run (loop);

	// clean up
	gst_element_set_state (pipeline, GST_STATE_NULL);
	gst_object_unref (pipeline);
	g_source_remove (watch_id);
	g_main_loop_unref (loop);

	return 0;
}
#endif
